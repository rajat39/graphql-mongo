const mongoose = require("mongoose");
const uri =
  "mongodb+srv://hyzen:imbatman@cluster0.lfvzu.mongodb.net/graph?retryWrites=true&w=majority";
const connectDB = async () => {
  const connection = await mongoose.connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  });

  console.log(`MongoDB Connected: ${connection.connection.host}`);
};

module.exports = connectDB;
