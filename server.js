const express = require("express");
const { ApolloServer, gql } = require("apollo-server-express");
const typeDefs = require("./typeDefs");
const resolvers = require("./resolvers");
const connectDB = require("./db");

async function startServer() {
  const app = express();
  connectDB();
  const apolloServer = new ApolloServer({
    typeDefs,
    resolvers,
  });

  await apolloServer.start();

  apolloServer.applyMiddleware({ app: app });

  app.use((req, res) => {
    res.send("Hello from express apoloose");
  });

  app.listen(4000, () => console.log("Server in running on port 4000"));
}

startServer();
